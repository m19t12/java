/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainApp;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Manolis
 */
public class MainScreen {

    public static void main(String[] args) {
        objInit();
    }

    private static void objInit() {
        XPathExpression expr;
        String id, widgetId, wslInstId = null;
        String[] propertys = null, propertysVal = null, propertysId;
        Node node, rootNode;
        NodeList nodeList;
        Element nodeElement;
        String classId, objName, containPrpId = null, className, partId, containPartId, containMthd, classId2;
        Object obj;
        Map<Integer, String> methodsOpt = new HashMap<Integer, String>();
        Map<String, Integer> mapOptIn = new HashMap<String, Integer>();
        Map<String, String> mapOptStr = new HashMap<String, String>();
        Document xmlDoc;
        XPath xPath;
        Map<String, Class> strToClass = new HashMap<String, Class>();

        Map<Class, Object> map = new HashMap<Class, Object>();

        strToClass.put("string", String.class);
        strToClass.put("int", Integer.TYPE);

        mapOptIn.put("DO NOTHING ON CLOSE", DO_NOTHING_ON_CLOSE);
        mapOptIn.put("HIDE ON CLOSE", HIDE_ON_CLOSE);
        mapOptIn.put("DISPOSE ON CLOSE", DISPOSE_ON_CLOSE);
        mapOptIn.put("EXIT ON CLOSE", EXIT_ON_CLOSE);

        xmlDoc = getDocument("xmlResrc/ui.xml");
        xPath = XPathFactory.newInstance().newXPath();
        try {
            expr = xPath.compile("uiModel/cuiModel/abstractContainer");
            rootNode = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
            System.out.println("--------------------");
            nodeElement = (Element) rootNode;
            id = nodeElement.getAttribute("id");
            widgetId = nodeElement.getAttribute("widgetId");
            System.out.println(widgetId);
            System.out.println(id);

            expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']");
            node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);

            if ("instance".equals(node.getFirstChild().getNextSibling().getNodeName())) {
                expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']/instance");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                wslInstId = nodeElement.getAttribute("wslInstId");
                System.out.println(wslInstId);

                if (node != null) {
                    expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']/instance/property");
                    nodeList = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);
                    propertys = new String[nodeList.getLength()];
                    propertysVal = new String[nodeList.getLength()];
                    for (int j = 0; j < nodeList.getLength(); j++) {
                        nodeElement = (Element) nodeList.item(j);
                        propertys[j] = nodeElement.getAttribute("wslPropId");
                        propertysVal[j] = nodeElement.getAttribute("value");
                        System.out.println(propertys[j]);
                        System.out.println(propertysVal[j]);
                    }
                }
            } else if ("property".equals(node.getFirstChild().getNextSibling().getNodeName())) {
                expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']/property");
                nodeList = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);

                propertys = new String[nodeList.getLength()];
                propertysVal = new String[nodeList.getLength()];
                propertysId = new String[nodeList.getLength()];

                for (int k = 0; k < nodeList.getLength(); k++) {
                    nodeElement = (Element) nodeList.item(k);
                    propertysVal[k] = nodeElement.getAttribute("value");
                    propertysId[k] = nodeElement.getAttribute("id");
                    System.out.println(propertysVal[k]);
                    System.out.println(propertysId[k]);
                }

                if (node != null) {
                    expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']/instance");
                    node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                    nodeElement = (Element) node;
                    wslInstId = nodeElement.getAttribute("wslInstId");
                    System.out.println(wslInstId);
                }
            }
            int length = propertys.length;
            String[] methodsId = new String[length], methodsType = new String[length], methodsName = new String[length];

            xmlDoc = getDocument("xmlResrc/abstractContainer.widget.xml");
            xPath = XPathFactory.newInstance().newXPath();

            System.out.println("----phase 2-----");

            expr = xPath.compile("/widget");
            node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
            nodeElement = (Element) node;
            // elenxos an psaxnoume sto sosto arxio
            if (nodeElement.getAttribute("id").equals(widgetId)) {
                // to class id gia na vroume ti sosti classi pou tha kalesoume
                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                classId = nodeElement.getAttribute("classId");
                //System.out.println(classId);

                // to onoma tou antikimenou
                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/display_name");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                objName = nodeElement.getTextContent();
                //System.out.println(objName);
                mapOptStr.put("Frame Title", objName);

                // an to antikimeno prepi na exei add method 
                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/containment/containmentProperty");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                if (node != null) {
                    nodeElement = (Element) node;
                    containPrpId = nodeElement.getAttribute("id");
                    System.out.println(containPrpId);

                    expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/mappings/mapping/mappingProperty[@id='" + containPrpId + "']/utilized[@in='method' and @asType = 'argument']");
                    node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                    nodeElement = (Element) node;
                    containPartId = nodeElement.getAttribute("partId");
                    classId2 = nodeElement.getAttribute("classId");
                    System.out.println(containPartId);

                    expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId2 + "']/method[@id='" + containPartId + "']");
                    node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                    nodeElement = (Element) node;
                    containMthd = nodeElement.getAttribute("name");
                    System.out.println(containMthd);
                }

                // an to antikimeno exei methodous
                if (propertys != null) {
                    for (int i = 0; i < propertys.length; i++) {
                        // to id tis methodou
                        expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/polymorphic_properties/property[@id='" + propertys[i] + "']");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        methodsId[i] = nodeElement.getAttribute("id");
                        //System.out.println(methodsId[i]);

                        // an h methodos exei options
                        if (node.getFirstChild() != null) {
                            expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/polymorphic_properties/property[@id='" + propertys[i] + "']/option");
                            nodeList = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);
                            for (int j = 0; j < nodeList.getLength(); j++) {
                                nodeElement = (Element) nodeList.item(j);
                                methodsOpt.put(Integer.valueOf(nodeElement.getAttribute("value")), nodeElement.getTextContent());
                            }
                        }
                        // to part id tis methodou gia na vroume to onoma tis methodou
                        expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/mappings/mapping/mappingProperty[@id='" + methodsId[i] + "']/utilized[@in='method' and @asType = 'argument']");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        partId = nodeElement.getAttribute("partId");
                        //System.out.println(partId);

                        // to onoma tis methodou
                        expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']/method[@id='" + partId + "']");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        methodsName[i] = nodeElement.getAttribute("name");
                        //System.out.println(methodsName[i]);

                        // o tipos tis methodou
                        expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']/method[@id='" + partId + "']/argument");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        methodsType[i] = nodeElement.getAttribute("type");
                        //System.out.println(methodsType[i]);
                    }
                }

                // to onoma tou antikimenou gia na kalesoume me reflection
                expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                className = nodeElement.getAttribute("path");
                System.out.println(className);

                // dimiourgia antikimenou
                Class cls = Class.forName(className);
                obj = cls.newInstance();

                Method[] methods = new Method[length];
                for (int i = 0; i < length; i++) {
                    methods[i] = cls.getMethod(methodsName[i], strToClass.get(methodsType[i]));
                    switch (methodsType[i]) {
                        case "int":
                            methods[i].invoke(obj, Integer.parseInt(propertysVal[i]));
                            break;
                        case "string":
                            methods[i].invoke(obj, mapOptStr.get(propertysVal[i]));
                            break;
                        default:
                            methods[i].invoke(obj);
                    }
                    System.out.println("-------------");
                    System.out.println(methodsName[i]);
                    System.out.println("-------------");
                }
                if (containPrpId != null && "javax.swing.JFrame".equals(className)) {
                    Container c = (Container) cls.getMethod("getContentPane").invoke(obj);

                    xmlDoc = getDocument("xmlResrc/ui.xml");
                    xPath = XPathFactory.newInstance().newXPath();
                    expr = xPath.compile("//abstractContainer/*");
                    node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                    c.add((Component) abObjects(node), BorderLayout.CENTER);

                    xmlDoc = getDocument("xmlResrc/ui.xml");
                    xPath = XPathFactory.newInstance().newXPath();
                    expr = xPath.compile("uiModel/cuiModel/abstractContainer/abstractContainer/abstractButton");
                    node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                    nodeElement = (Element) node;
                    System.out.println(nodeElement.getAttribute("id") + " OBJECT");
                    c.add((Component) abObjects(node), BorderLayout.SOUTH);
                    
                    Method visible = cls.getMethod("setVisible", Boolean.TYPE);
                    visible.invoke(obj, true);
                    cls.getMethod("pack").invoke(obj);
                }
            } else {
                xmlDoc = getDocument("xmlResrc/AbstractButton.widget.xml");
                xPath = XPathFactory.newInstance().newXPath();

                expr = xPath.compile("/widget");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;

                // to onoma tou antikimenou
                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/display_name");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                objName = nodeElement.getTextContent();
                System.out.println(objName);

                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                classId = nodeElement.getAttribute("classId");
                System.out.println(classId);

                // to onoma tou antikimenou gia na kalesoume me reflection
                expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                className = nodeElement.getAttribute("path");
                System.out.println(className);

                // dimiourgia antikimenou
                Class cls = Class.forName(className);
                obj = cls.getDeclaredConstructor(String.class).newInstance(objName);

            }
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | XPathExpressionException ex) {
            Logger.getLogger(MainScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Object abObjects(Node rootNode) {

        XPathExpression expr;
        String id, widgetId, wslInstId = null;
        String[] propertys = null, propertysVal = null, propertysId;
        Node node;
        NodeList nodeList;
        Element nodeElement;
        String classId, objName, containPrpId = null, className, partId, containPartId, containMthd, classId2, buttonProp = "", buttonPropVal = null;
        String abObjName = null;
        Object obj = null;
        Map<Integer, String> methodsOpt = new HashMap<Integer, String>();
        Map<String, Integer> mapOptIn = new HashMap<String, Integer>();
        Map<String, String> mapOptStr = new HashMap<String, String>();
        Document xmlDoc;
        XPath xPath;
        Map<String, Class> strToClass = new HashMap<String, Class>();

        strToClass.put("string", String.class);
        strToClass.put("int", Integer.TYPE);

        mapOptIn.put("DO NOTHING ON CLOSE", DO_NOTHING_ON_CLOSE);
        mapOptIn.put("HIDE ON CLOSE", HIDE_ON_CLOSE);
        mapOptIn.put("DISPOSE ON CLOSE", DISPOSE_ON_CLOSE);
        mapOptIn.put("EXIT ON CLOSE", EXIT_ON_CLOSE);

        xmlDoc = getDocument("xmlResrc/ui.xml");
        xPath = XPathFactory.newInstance().newXPath();
        try {
            System.out.println("--------------------");
            nodeElement = (Element) rootNode;
            id = nodeElement.getAttribute("id");
            widgetId = nodeElement.getAttribute("widgetId");
            System.out.println(widgetId);
            System.out.println(id);

            expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']");
            node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);

            if ("instance".equals(node.getFirstChild().getNextSibling().getNodeName())) {
                expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']/instance");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                wslInstId = nodeElement.getAttribute("wslInstId");
                System.out.println(wslInstId);

                if (node != null) {
                    expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']/instance/property");
                    nodeList = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);
                    propertys = new String[nodeList.getLength()];
                    propertysVal = new String[nodeList.getLength()];
                    for (int j = 0; j < nodeList.getLength(); j++) {
                        nodeElement = (Element) nodeList.item(j);
                        propertys[j] = nodeElement.getAttribute("wslPropId");
                        propertysVal[j] = nodeElement.getAttribute("value");
                        System.out.println(propertys[j]);
                        System.out.println(propertysVal[j]);
                    }
                }
            } else if ("property".equals(node.getFirstChild().getNextSibling().getNodeName())) {
                expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']/property");
                nodeList = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);

                propertys = new String[nodeList.getLength()];
                propertysVal = new String[nodeList.getLength()];
                propertysId = new String[nodeList.getLength()];
                
                
                for (int k = 0; k < nodeList.getLength(); k++) {
                    nodeElement = (Element) nodeList.item(k);
                    
                    abObjName = nodeElement.getAttribute("value");
                    propertys[k] = nodeElement.getAttribute("wslPropId");
                    propertysId[k] = nodeElement.getAttribute("id");
                    
                    System.out.println(propertysVal[k]);
                    System.out.println(propertysId[k]);
                    System.out.println(propertys[k]);
                }

                if (node != null) {
                    expr = xPath.compile("/uiModel/widgetResourceModel/widgetResource[@cioId = '" + id + "']/instance");
                    node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                    nodeElement = (Element) node;
                    wslInstId = nodeElement.getAttribute("wslInstId");
                    System.out.println(wslInstId);
                    if (node.getFirstChild() != null) {
                        System.out.println(node.getFirstChild().getNextSibling().getNodeName());
                        nodeElement = (Element) node.getFirstChild().getNextSibling();
                        buttonProp = nodeElement.getAttribute("wslPropId");
                        buttonPropVal = nodeElement.getAttribute("value");
                        System.out.println(buttonProp);
                    }
                }
            }
            int length = propertys.length;
            String[] methodsId = new String[length], methodsType = new String[length], methodsName = new String[length];

            xmlDoc = getDocument("xmlResrc/abstractContainer.widget.xml");
            xPath = XPathFactory.newInstance().newXPath();

            System.out.println("----phase 2-----");

            expr = xPath.compile("/widget");
            node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
            nodeElement = (Element) node;
            // elenxos an psaxnoume sto sosto arxio
            if (nodeElement.getAttribute("id").equals(widgetId)) {
                // to class id gia na vroume ti sosti classi pou tha kalesoume
                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                classId = nodeElement.getAttribute("classId");
                //System.out.println(classId);

                // to onoma tou antikimenou
                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/display_name");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                objName = nodeElement.getTextContent();
                //System.out.println(objName);
                mapOptStr.put("Frame Title", objName);

                // an to antikimeno prepi na exei add method 
                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/containment/containmentProperty");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                if (node != null) {
                    nodeElement = (Element) node;
                    containPrpId = nodeElement.getAttribute("id");
                    System.out.println(containPrpId);

                    expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/mappings/mapping/mappingProperty[@id='" + containPrpId + "']/utilized[@in='method' and @asType = 'argument']");
                    node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                    nodeElement = (Element) node;
                    containPartId = nodeElement.getAttribute("partId");
                    classId2 = nodeElement.getAttribute("classId");
                    System.out.println(containPartId);

                    expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId2 + "']/method[@id='" + containPartId + "']");
                    node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                    nodeElement = (Element) node;
                    containMthd = nodeElement.getAttribute("name");
                    System.out.println(containMthd);
                }

                // an to antikimeno exei methodous
                if (propertys != null) {
                    for (int i = 0; i < propertys.length; i++) {
                        // to id tis methodou
                        expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/polymorphic_properties/property[@id='" + propertys[i] + "']");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        methodsId[i] = nodeElement.getAttribute("id");
                        //System.out.println(methodsId[i]);

                        // an h methodos exei options
                        if (node.getFirstChild() != null) {
                            expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/polymorphic_properties/property[@id='" + propertys[i] + "']/option");
                            nodeList = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);
                            for (int j = 0; j < nodeList.getLength(); j++) {
                                nodeElement = (Element) nodeList.item(j);
                                methodsOpt.put(Integer.valueOf(nodeElement.getAttribute("value")), nodeElement.getTextContent());
                            }
                        }
                        // to part id tis methodou gia na vroume to onoma tis methodou
                        expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/mappings/mapping/mappingProperty[@id='" + methodsId[i] + "']/utilized[@in='method' and @asType = 'argument']");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        partId = nodeElement.getAttribute("partId");
                        //System.out.println(partId);

                        // to onoma tis methodou
                        expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']/method[@id='" + partId + "']");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        methodsName[i] = nodeElement.getAttribute("name");
                        //System.out.println(methodsName[i]);

                        // o tipos tis methodou
                        expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']/method[@id='" + partId + "']/argument");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        methodsType[i] = nodeElement.getAttribute("type");
                        //System.out.println(methodsType[i]);
                    }
                }

                // to onoma tou antikimenou gia na kalesoume me reflection
                expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                className = nodeElement.getAttribute("path");
                System.out.println(className);

                // dimiourgia antikimenou
                Class cls = Class.forName(className);
                obj = cls.newInstance();

                Method[] methods = new Method[length];
                for (int i = 0; i < length; i++) {
                    methods[i] = cls.getMethod(methodsName[i], strToClass.get(methodsType[i]));
                    switch (methodsType[i]) {
                        case "int":
                            methods[i].invoke(obj, Integer.parseInt(propertysVal[i]));
                            break;
                        case "string":
                            methods[i].invoke(obj, mapOptStr.get(propertysVal[i]));
                            break;
                        default:
                            methods[i].invoke(obj);
                    }
                    System.out.println("-------------");
                    System.out.println(methodsName[i]);
                    System.out.println("-------------");
                }
                if ("javax.swing.JPanel".equals(className) && containPrpId != null) {
                    xmlDoc = getDocument("xmlResrc/ui.xml");
                    xPath = XPathFactory.newInstance().newXPath();
                    expr = xPath.compile("//abstractContainer/*/*/*");
                    nodeList = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);
                    System.out.println(nodeList.getLength() + " PANEL");
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        nodeElement = (Element) nodeList.item(i);
                        System.out.println(nodeElement.getAttribute("id"));
                        cls.getMethod("add", Component.class).invoke(obj, abObjects(nodeList.item(i)));
                    }
                }
            } else {
                xmlDoc = getDocument("xmlResrc/AbstractButton.widget.xml");
                xPath = XPathFactory.newInstance().newXPath();

                expr = xPath.compile("/widget");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;

                // to onoma tou antikimenou
                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/display_name");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                objName = nodeElement.getTextContent();
                System.out.println(objName);

                expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                classId = nodeElement.getAttribute("classId");
                System.out.println(classId);

                // to onoma tou antikimenou gia na kalesoume me reflection
                expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']");
                node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                nodeElement = (Element) node;
                className = nodeElement.getAttribute("path");
                System.out.println(className);

                // an to antikimeno exei methodous
                if (buttonProp != "") {
                    for (int i = 0; i < propertys.length; i++) {
                        // to id tis methodou
                        expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/polymorphic_properties/property[@id='" + buttonProp + "']");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        methodsId[i] = nodeElement.getAttribute("id");
                        //System.out.println(methodsId[i]);

                        // an h methodos exei options
                        if (node.getFirstChild() != null) {
                            expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/polymorphic_properties/property[@id='" + buttonProp + "']/option");
                            nodeList = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);
                            for (int j = 0; j < nodeList.getLength(); j++) {
                                nodeElement = (Element) nodeList.item(j);
                                methodsOpt.put(Integer.valueOf(nodeElement.getAttribute("value")), nodeElement.getTextContent());
                            }
                        }
                        // to part id tis methodou gia na vroume to onoma tis methodou
                        expr = xPath.compile("/widget/instances/instance[@id='" + wslInstId + "']/mappings/mapping/mappingProperty[@id='" + methodsId[i] + "']/utilized[@in='method' and @asType = 'argument']");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        partId = nodeElement.getAttribute("partId");
                        //System.out.println(partId);

                        // to onoma tis methodou
                        expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']/method[@id='" + partId + "']");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        methodsName[i] = nodeElement.getAttribute("name");
                        //System.out.println(methodsName[i]);

                        // o tipos tis methodou
                        expr = xPath.compile("/widget/instances/instance/instance_api/class[@id='" + classId + "']/method[@id='" + partId + "']/argument");
                        node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
                        nodeElement = (Element) node;
                        methodsType[i] = nodeElement.getAttribute("type");
                        //System.out.println(methodsType[i]);
                    }
                }

                // dimiourgia antikimenou
                Class cls = Class.forName(className);
                obj = cls.getDeclaredConstructor(String.class).newInstance(abObjName);

                if (buttonProp != "") {
                    Method[] methods = new Method[length];
                    for (int i = 0; i < length; i++) {
                        System.out.println(methodsType[i]);
                        System.out.println(methodsName[i]);
                        methods[i] = cls.getMethod(methodsName[i], strToClass.get(methodsType[i]));
                        switch (methodsType[i]) {
                            case "int":
                                methods[i].invoke(obj, Integer.parseInt(buttonPropVal));
                                break;
                            case "string":
                                methods[i].invoke(obj, mapOptStr.get(buttonPropVal));
                                break;
                            default:
                                methods[i].invoke(obj);
                        }
                        System.out.println("-------------");
                        System.out.println(methodsName[i]);
                        System.out.println("-------------");
                    }
                }
            }
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | XPathExpressionException ex) {
            Logger.getLogger(MainScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    private static Document getDocument(String docString) {
        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringComments(true);
            factory.setIgnoringElementContentWhitespace(true);
            factory.setValidating(true);

            DocumentBuilder builder = factory.newDocumentBuilder();

            return builder.parse(new InputSource(docString));

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
